<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreKaryawanRequest;
use App\Http\Requests\UpdateKaryawanRequest;
use App\Models\Session;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $karyawans = Karyawan::where('is_winner', FALSE)->select('id', 'nama_pt', 'nama_karyawan')->get();
        return $karyawans;
    }

    public function randomKaryawan()
    {
        $session_name = Session::where('is_active', TRUE)->first()->session_name;
        if ($session_name == 'Sesi 1 (bagian 1)') {
            $jumlah_hadiah = 20;
        } else if ($session_name == 'Sesi 1 (bagian 2)') {
            $jumlah_hadiah = 20;
        } else if ($session_name == 'Sesi 2 (bagian 1)') {
            $jumlah_hadiah = 20;
        } else if ($session_name == 'Sesi 2 (bagian 2)') {
            $jumlah_hadiah = 20;
        } else if ($session_name == 'Sesi 3 (bagian 1)') {
            $jumlah_hadiah = 15;
        } else if ($session_name == 'Sesi 3 (bagian 2)') {
            $jumlah_hadiah = 15;
        } else if ($session_name == 'Grandprize') {
            $jumlah_hadiah = 1;
        }
        $random_karyawan = Karyawan::where('is_winner', FALSE)->orderBy(DB::raw('RAND()'))->limit($jumlah_hadiah)->select('id', 'nama_karyawan', 'nama_pt', 'is_winner')->get();
        return $random_karyawan;
    }

    public function eliminasi(Request $request)
    {
        $karyawan = Karyawan::find($request->id_karyawan)->nama_karyawan;

        Karyawan::find($request->id_karyawan)->update([
            'is_winner' => 2
        ]);

        return response()->json([
            'status' => 'success',
            'message' => $karyawan . ' telah ter-eliminasi'
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreKaryawanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreKaryawanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function show(Karyawan $karyawan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function edit(Karyawan $karyawan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateKaryawanRequest  $request
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateKaryawanRequest $request, Karyawan $karyawan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Karyawan  $karyawan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Karyawan::destroy($request->id_karyawan);

        return response()->json([
            'status' => 'sukses',
            'message' => 'berhasil menghapus data'
        ]);
    }
}
