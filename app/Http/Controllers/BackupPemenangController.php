<?php

namespace App\Http\Controllers;

use App\Models\BackupPemenang;
use App\Http\Requests\StoreBackupPemenangRequest;
use App\Http\Requests\UpdateBackupPemenangRequest;

class BackupPemenangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBackupPemenangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBackupPemenangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BackupPemenang  $backupPemenang
     * @return \Illuminate\Http\Response
     */
    public function show(BackupPemenang $backupPemenang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BackupPemenang  $backupPemenang
     * @return \Illuminate\Http\Response
     */
    public function edit(BackupPemenang $backupPemenang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBackupPemenangRequest  $request
     * @param  \App\Models\BackupPemenang  $backupPemenang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBackupPemenangRequest $request, BackupPemenang $backupPemenang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BackupPemenang  $backupPemenang
     * @return \Illuminate\Http\Response
     */
    public function destroy(BackupPemenang $backupPemenang)
    {
        //
    }
}
