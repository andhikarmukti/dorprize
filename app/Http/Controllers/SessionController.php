<?php

namespace App\Http\Controllers;

use App\Models\Session;
use App\Http\Requests\StoreSessionRequest;
use App\Http\Requests\UpdateSessionRequest;
use App\Models\Hadiah;
use App\Models\Karyawan;
use App\Models\Pemenang;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $session_name = Session::where('is_active', TRUE)->select('session_name', 'jumlah_hadiah')->get()
            ->map(function ($session_name) {
                if ($session_name->session_name == 'Grandprize') {
                    $session_name->jumlah_hadiah = 1;
                    return $session_name;
                }
                return $session_name;
            })->first();
        return $session_name;
    }

    public function next()
    {
        $karyawan_count = Karyawan::where('is_winner', TRUE)->count();
        $session_name = Session::where('is_active', TRUE)->first()->session_name;
        $sisa_hadiah = Hadiah::where('is_gifted', FALSE)->get()->count();

        if ($sisa_hadiah == 0) {
            Session::find(8)->update([
                'is_active' => TRUE
            ]);

            Session::find(7)->update([
                'is_active' => FALSE
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'berhasil melakukan update ke ' . $session_name,
                'sisa hadiah' => $sisa_hadiah
            ], 200);
        } else if ($karyawan_count < 20) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 1) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 20 && $karyawan_count < 40) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 2) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 40 && $karyawan_count < 60) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 3) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 60 && $karyawan_count < 80) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 4) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 80 && $karyawan_count < 95) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 5) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 95 && $karyawan_count < 110) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 6) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        } else if ($karyawan_count >= 110 && $karyawan_count <= 120) {
            for ($i = 1; $i < 8; $i++) {
                if ($i == 7) {
                    Session::find($i)->update([
                        'is_active' => TRUE
                    ]);
                } else {
                    Session::find($i)->update([
                        'is_active' => FALSE
                    ]);
                }
            }
        }

        // return Karyawan::where('is_winner', TRUE)->get()->count();
        if ($session_name == 'Grandprize') {
            $sisa_hadiah = Hadiah::where('is_gifted', FALSE)->limit(1)->get()->count();
        }

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil melakukan update ke ' . $session_name,
            'sisa hadiah' => $sisa_hadiah
        ], 200);
    }

    public function resetSession()
    {
        for ($i = 1; $i <= 8; $i++) {
            if ($i == 1) {
                Session::find($i)->update([
                    'is_active' => TRUE
                ]);
            } else {
                Session::find($i)->update([
                    'is_active' => FALSE
                ]);
            }
        }

        $karyawan_length = Karyawan::all()->count();
        for ($i = 1; $i <= $karyawan_length; $i++) {
            Karyawan::find($i)->update([
                'is_winner' => FALSE
            ]);
        }

        $hadiah_length = Hadiah::all()->count();
        for ($i = 1; $i <= $hadiah_length; $i++) {
            Hadiah::find($i)->update([
                'is_gifted' => FALSE
            ]);
        }

        Pemenang::truncate();

        return response()->json([
            'status' => 'success',
            'message' => 'berhasil melakukan reset session, dan data pemenang'
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSessionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSessionRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function show(Session $session)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function edit(Session $session)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSessionRequest  $request
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSessionRequest $request, Session $session)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Session  $session
     * @return \Illuminate\Http\Response
     */
    public function destroy(Session $session)
    {
        //
    }
}
