<?php

namespace App\Http\Controllers;

use App\Models\Hadiah;
use App\Http\Requests\StoreHadiahRequest;
use App\Http\Requests\UpdateHadiahRequest;
use App\Models\Session;
use Illuminate\Http\Request;

class HadiahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $session_name = Session::where('is_active', TRUE)->first()->session_name;

        if ($session_name == 'Grandprize') {
            $hadiahs = Hadiah::where('is_gifted', FALSE)
                ->limit(1)
                ->select('id', 'nama_hadiah', 'gambar')
                ->orderBy('id', 'DESC')
                ->get()
                ->map(function ($hadiahs) {
                    $hadiahs->gambar = '/img/hadiah/' . trim($hadiahs->gambar);
                    return $hadiahs;
                });
        } else {
            $hadiahs = Hadiah::where('is_gifted', FALSE)
                ->select('id', 'nama_hadiah', 'gambar')
                ->orderBy('id', 'DESC')
                ->get()
                ->map(function ($hadiahs) {
                    $hadiahs->gambar = '/img/hadiah/' . trim($hadiahs->gambar);
                    return $hadiahs;
                });
        }

        return $hadiahs;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreHadiahRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHadiahRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Hadiah  $hadiah
     * @return \Illuminate\Http\Response
     */
    public function show(Hadiah $hadiah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Hadiah  $hadiah
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $hadiah = Hadiah::find($request->id_hadiah);
        if (!$hadiah) {
            return response()->json([
                'status' => 'Failed',
                'message' => 'Data not found'
            ], 404);
        }

        $hadiah->update([
            'is_gifted' => TRUE
        ]);

        return response()->json([
            'data' => $hadiah,
            'status' => 'Success Deleted'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateHadiahRequest  $request
     * @param  \App\Models\Hadiah  $hadiah
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHadiahRequest $request, Hadiah $hadiah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Hadiah  $hadiah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Hadiah::destroy($request->id_hadiah);
    }
}
