<?php

namespace App\Http\Controllers;

use App\Models\Pemenang;
use App\Http\Requests\StorePemenangRequest;
use App\Http\Requests\UpdatePemenangRequest;
use App\Models\BackupPemenang;
use App\Models\Hadiah;
use App\Models\Karyawan;
use Illuminate\Http\Request;

class PemenangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pemenang::all()->makeHidden(['created_at', 'updated_at']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePemenangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        if ($request->id_karyawan == null || $request->id_hadiah == null) {
            return response()->json([
                'status' => 'Failed',
                'message' => 'Parameter cant be null'
            ], 403);
        }

        if (Pemenang::where('id_karyawan', $request->id_karyawan)->first()) {
            return response()->json([
                'status' => 'Failed',
                'message' => 'Karyawan tersebut sudah memenangkan hadiah'
            ], 403);
        }

        if (Pemenang::where('id_hadiah', $request->id_hadiah)->first()) {
            return response()->json([
                'status' => 'Failed',
                'message' => 'Hadiah tersebut sudah dimenangkan oleh keryawan'
            ], 403);
        }

        Pemenang::create([
            'id_karyawan' => $request->id_karyawan,
            'id_hadiah' => $request->id_hadiah,
        ]);

        BackupPemenang::create([
            'id_karyawan' => $request->id_karyawan,
            'id_hadiah' => $request->id_hadiah,
        ]);

        Karyawan::find($request->id_karyawan)
            ->update([
                'is_winner' => TRUE
            ]);

        Hadiah::find($request->id_hadiah)
            ->update([
                'is_gifted' => TRUE
            ]);
        $nama_karyawan = Karyawan::find($request->id_karyawan)->nama_karyawan;
        $nama_hadiah   = Hadiah::find($request->id_hadiah)->nama_hadiah;

        return response()->json([
            'status' => 'sukses',
            'message' => 'Berhasil menambahkan data pemenang',
            'data' => [
                'karyawan' => $nama_karyawan,
                'hadiah' => $nama_hadiah
            ]
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function show(Pemenang $pemenang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function edit(Pemenang $pemenang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePemenangRequest  $request
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePemenangRequest $request, Pemenang $pemenang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pemenang  $pemenang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pemenang $pemenang)
    {
        //
    }
}
