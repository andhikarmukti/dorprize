<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hadiah extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function pemenang()
    {
        return $this->belongsTo(Pemenang::class);
    }
}
