<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\HadiahSeeder;
use Database\Seeders\SessionSeeder;
use Database\Seeders\KaryawanSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // $this->call(KaryawanSeeder::class);
        $this->call(HadiahSeeder::class);
        $this->call(SessionSeeder::class);
    }
}
