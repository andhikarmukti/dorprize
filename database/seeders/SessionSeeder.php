<?php

namespace Database\Seeders;

use App\Models\Session;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Session::create([
            'session_name' => 'Session 1',
            'jumlah_hadiah' => 40
        ]);
        Session::create([
            'session_name' => 'Session 2',
            'jumlah_hadiah' => 40
        ]);
        Session::create([
            'session_name' => 'Session 3',
            'jumlah_hadiah' => 30
        ]);
        Session::create([
            'session_name' => 'Grandprize',
            'jumlah_hadiah' => 10
        ]);

        Session::find(1)->update([
            'is_active' => 1
        ]);
    }
}
