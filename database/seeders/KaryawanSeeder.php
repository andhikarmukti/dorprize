<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\Karyawan;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class KaryawanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Andhika'
        ]);
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Farhan'
        ]);
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Roby'
        ]);
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Nurul'
        ]);
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Yusril'
        ]);
        Karyawan::create([
            'nama_pt' => 'PT. Artapuri Digital Mediatama',
            'nama_karyawan' => 'Prisilia'
        ]);

        Karyawan::factory(94)->create();
    }
}
