<?php

namespace Database\Seeders;

use App\Models\Hadiah;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class HadiahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hadiah::create([
            'nama_hadiah' => 'Magic.com'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Blender'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kursi Pijat'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Monitor LG'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Botol Minum'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sepatu Futsal'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kandang Hamster'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Ban dalem swallow'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Keyboard DA Gaming'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Cashing Iphone 13 Pro'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Kuota 8GB'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Seperangkat alat solat'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sajadah Terbang'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Lampu Jin'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kucing Anggora'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Mobil Fortuner'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Pasir kucing'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kunci inggris'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Mouse Razer'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Jam tangan'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Satu set drum'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gitar listrik'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sound System'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Honda beat second'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'PC gaming'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Bola futsal'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Net volly'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Antena TV'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kartu Remi'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kartu Gaple'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Mouse pad bekas warnet'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Hordeng'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Topi'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Singlet'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Baju gambar sponge bob'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher belanja indomart 500.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gantungan kunci'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Poster BTS'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher grab gratis ongkir'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'CD Noah'
        ]);

        Hadiah::create([
            'nama_hadiah' => '1000 instagram followers'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Chitato 2 dus'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Jaipong item 4 renteng'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Pulsa 50.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Keset wellcome'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Bed cover'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis makan siang di Mcd 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Xiaomi Redmi Note 8'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Iphone 13'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Karbu Honda Beat'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'AC bekas merek Samsung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Headset Steel Series'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Meja belajar anak'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kompor gas elektrik'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Token listrik 1juta'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis Wifi Indihouse 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Tas gunung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher Gopay 500.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Nokia 3315'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Setrika Uap'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Magic.com'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Blender'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kursi Pijat'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Monitor LG'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Botol Minum'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sepatu Futsal'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kandang Hamster'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Ban dalem swallow'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Keyboard DA Gaming'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Cashing Iphone 13 Pro'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Kuota 8GB'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Seperangkat alat solat'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sajadah Terbang'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Lampu Jin'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kucing Anggora'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Mobil Fortuner'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Pasir kucing'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kunci inggris'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Mouse Razer'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Jam tangan'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Satu set drum'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gitar listrik'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Sound System'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Honda beat second'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'PC gaming'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Bola futsal'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Net volly'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Antena TV'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kartu Remi'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kartu Gaple'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'Mouse pad bekas warnet'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Hordeng'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Topi'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Singlet'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Baju gambar sponge bob'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher belanja indomart 500.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gantungan kunci'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Poster BTS'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher grab gratis ongkir'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'CD Noah'
        ]);

        Hadiah::create([
            'nama_hadiah' => '1000 instagram followers'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Chitato 2 dus'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Jaipong item 4 renteng'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Pulsa 50.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Keset wellcome'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Bed cover'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis makan siang di Mcd 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Xiaomi Redmi Note 8'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Iphone 13'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Karbu Honda Beat'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'AC bekas merek Samsung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Headset Steel Series'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Meja belajar anak'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kompor gas elektrik'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Token listrik 1juta'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis Wifi Indihouse 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Tas gunung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher Gopay 500.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Nokia 3315'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Setrika Uap'
        ]);

        Hadiah::create([
            'nama_hadiah' => '1000 instagram followers'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Chitato 2 dus'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Jaipong item 4 renteng'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Pulsa 50.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Keset wellcome'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Bed cover'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis makan siang di Mcd 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Xiaomi Redmi Note 8'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Iphone 13'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Karbu Honda Beat'
        ]);

        Hadiah::create([
            'nama_hadiah' => 'AC bekas merek Samsung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Headset Steel Series'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Meja belajar anak'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Kompor gas elektrik'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Token listrik 1juta'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Gratis Wifi Indihouse 1 bulan'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Tas gunung'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Voucher Gopay 500.000'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Nokia 3315'
        ]);
        Hadiah::create([
            'nama_hadiah' => 'Setrika Uap'
        ]);
    }
}
