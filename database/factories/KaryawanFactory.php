<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Karyawan>
 */
class KaryawanFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nama_karyawan' => $this->faker->name(),
            'nama_pt' => Arr::random(['PT. Krakatau Daya Listrik', 'PT. Artapuri Digital Mediatama'])
        ];
    }
}
