<?php

use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HadiahController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\PemenangController;
use App\Http\Controllers\SessionController;
use App\Models\Hadiah;
use App\Models\Karyawan;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Karyawan
Route::get('/karyawans', [KaryawanController::class, 'index']);
// Route::delete('/karyawan', [KaryawanController::class, 'destroy']);

// Hadiah
Route::get('/hadiahs', [HadiahController::class, 'index']);
// Route::put('/hadiah', [HadiahController::class, 'edit']);

// Pemanang
Route::get('/pemenang', [PemenangController::class, 'index']);
Route::post('/pemenang', [PemenangController::class, 'store']);

// Session
Route::get('/session', [SessionController::class, 'index']);
Route::get('/next', [SessionController::class, 'next']);
Route::get('/reset', [SessionController::class, 'resetSession']);

// Random Karyawan
Route::get('/random_karyawan', [KaryawanController::class, 'randomKaryawan']);

// Eliminasi
Route::post('/eliminasi', [KaryawanController::class, 'eliminasi']);

// Route::get('/tes', function () {
//     $name = [];
//     for ($i = 0; $i < 50; $i++) {
//         $randomName = Factory::create()->name(1);

//         // array_push($name, $randomName);
//     }
//     return $randomNIP = random_int(10, 12345678910);;
// });

// Route::get('/gambar', function () {
//     $count_hadiah = Hadiah::all()->count();
//     for ($i = 1; $i <= $count_hadiah; $i++) {
//         // if ($i > 110) {
//         //     Hadiah::find($i)->update([
//         //         'gambar' => 'kado.png'
//         //     ]);
//         // }
//         Hadiah::find($i)->update([
//             'gambar' => 'kado.png'
//         ]);
//     }
// });
